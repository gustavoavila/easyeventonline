package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.UsuarioController;
import br.com.easyevent.domain.Usuario;
import br.com.easyevent.resources.UsuarioResource;

public class UsuarioResourceAssembler extends ResourceAssemblerSupport<Usuario, UsuarioResource> {

	public UsuarioResourceAssembler() {
		super(UsuarioController.class, UsuarioResource.class);
	}

	@Override
	protected UsuarioResource instantiateResource(Usuario usuario) {
		return new UsuarioResource(usuario);
	}

	@Override
	public UsuarioResource toResource(Usuario usuario) {
		return createResourceWithId(usuario.getId(), usuario);
	}

}
