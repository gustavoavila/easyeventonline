package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.EmpresaController;
import br.com.easyevent.domain.Empresa;
import br.com.easyevent.resources.EmpresaResource;

public class EmpresaResourceAssembler extends ResourceAssemblerSupport<Empresa, EmpresaResource> {

	public EmpresaResourceAssembler() {
		super(EmpresaController.class, EmpresaResource.class);
	}

	@Override
	protected EmpresaResource instantiateResource(Empresa empresa) {
		return new EmpresaResource(empresa);
	}

	@Override
	public EmpresaResource toResource(Empresa empresa) {
		return createResourceWithId(empresa.getId(), empresa);
	}

}
