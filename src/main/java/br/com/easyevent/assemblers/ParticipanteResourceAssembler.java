package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.ParticipanteController;
import br.com.easyevent.domain.Participante;
import br.com.easyevent.resources.ParticipanteResource;

public class ParticipanteResourceAssembler extends ResourceAssemblerSupport<Participante, ParticipanteResource> {

	public ParticipanteResourceAssembler() {
		super(ParticipanteController.class, ParticipanteResource.class);
	}

	@Override
	public ParticipanteResource toResource(Participante participante) {
		return createResourceWithId(participante.getId(), participante);
	}

}
