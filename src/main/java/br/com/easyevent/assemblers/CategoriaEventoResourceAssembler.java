package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.CategoriaEventoController;
import br.com.easyevent.domain.CategoriaEvento;
import br.com.easyevent.resources.CategoriaEventoResource;

public class CategoriaEventoResourceAssembler extends ResourceAssemblerSupport<CategoriaEvento, CategoriaEventoResource> {

	public CategoriaEventoResourceAssembler() {
		super(CategoriaEventoController.class, CategoriaEventoResource.class);
	}
	
	@Override
	protected CategoriaEventoResource instantiateResource(CategoriaEvento categoriaEvento) {
		return new CategoriaEventoResource(categoriaEvento);
	}

	@Override
	public CategoriaEventoResource toResource(CategoriaEvento categoriaEvento) {
		return createResourceWithId(categoriaEvento.getId(), categoriaEvento);
	}

	
}
