package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.EnderecoController;
import br.com.easyevent.domain.Endereco;
import br.com.easyevent.resources.EnderecoResource;

public class EnderecoResourceAssembler extends ResourceAssemblerSupport<Endereco, EnderecoResource> {

	public EnderecoResourceAssembler() {
		super(EnderecoController.class, EnderecoResource.class);
	}

	@Override
	public EnderecoResource toResource(Endereco endereco) {
		return createResourceWithId(endereco.getId(), endereco);
	}
	
	@Override
	protected EnderecoResource instantiateResource(Endereco endereco) {
		return new EnderecoResource(endereco);
	}
}
