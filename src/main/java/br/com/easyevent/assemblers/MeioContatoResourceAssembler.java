package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.MeioContatoController;
import br.com.easyevent.domain.MeioContato;
import br.com.easyevent.resources.MeioContatoResource;

public class MeioContatoResourceAssembler 
	extends ResourceAssemblerSupport<MeioContato, MeioContatoResource> {

	public MeioContatoResourceAssembler() {
		super(MeioContatoController.class, MeioContatoResource.class);
	}

	@Override
	public MeioContatoResource toResource(MeioContato meioContato) {
		return createResourceWithId(meioContato.getId(), meioContato);
	}
	
	@Override
	protected MeioContatoResource instantiateResource(MeioContato meioContato) {
		return new MeioContatoResource(meioContato);
	}

}
