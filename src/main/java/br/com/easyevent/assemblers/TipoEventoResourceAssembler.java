package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.TipoEventoController;
import br.com.easyevent.domain.TipoEvento;
import br.com.easyevent.resources.TipoEventoResource;

public class TipoEventoResourceAssembler extends ResourceAssemblerSupport<TipoEvento, TipoEventoResource> {

	public TipoEventoResourceAssembler() {
		super(TipoEventoController.class, TipoEventoResource.class);
	}

	@Override
	protected TipoEventoResource instantiateResource(TipoEvento tipoEvento) {
		return new TipoEventoResource(tipoEvento);
	}
	
	@Override
	public TipoEventoResource toResource(TipoEvento tipoEvento) {
		return createResourceWithId(tipoEvento.getId(), tipoEvento);
	}

}
