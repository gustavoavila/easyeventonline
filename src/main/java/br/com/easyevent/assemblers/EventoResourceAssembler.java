package br.com.easyevent.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import br.com.easyevent.controllers.EventoController;
import br.com.easyevent.domain.Evento;
import br.com.easyevent.resources.EventoResource;

public class EventoResourceAssembler extends ResourceAssemblerSupport<Evento, EventoResource> {

	public EventoResourceAssembler() {
		super(EventoController.class, EventoResource.class);
	}
	
	@Override
	protected EventoResource instantiateResource(Evento evento) {
		return new EventoResource(evento);
	}

	@Override
	public EventoResource toResource(Evento evento) {
		return createResourceWithId(evento.getId(), evento);
	}

}
