package br.com.easyevent.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.easyevent.domain.Cidade;
import br.com.easyevent.repositories.CidadeRepository;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;
	
	public Cidade findByCodigoIBGE(Integer codigo) {
		return cidadeRepository.findByCodigoIBGE(codigo);
	}

}
