package br.com.easyevent.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.easyevent.domain.Usuario;
import br.com.easyevent.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private BCryptPasswordEncoder passEncoder;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public Usuario save(Usuario usuario) {
		usuario.setId(null);
		String senhaEncoded = passEncoder.encode(usuario.getSenha());
		usuario.setSenha(senhaEncoded);
		usuario = usuarioRepository.save(usuario);
		return usuario;
	}
}
