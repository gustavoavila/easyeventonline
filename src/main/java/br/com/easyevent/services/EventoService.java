package br.com.easyevent.services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.easyevent.domain.Cidade;
import br.com.easyevent.domain.Empresa;
import br.com.easyevent.domain.Endereco;
import br.com.easyevent.domain.Evento;
import br.com.easyevent.domain.MeioContato;
import br.com.easyevent.domain.TipoEvento;
import br.com.easyevent.domain.TipoMeioContato;
import br.com.easyevent.dto.EventoDTO;
import br.com.easyevent.repositories.CidadeRepository;
import br.com.easyevent.repositories.EmpresaRepository;
import br.com.easyevent.repositories.EnderecoRepository;
import br.com.easyevent.repositories.EventoRepository;
import br.com.easyevent.repositories.MeioContatoRepository;
import br.com.easyevent.repositories.TipoEventoRepository;
import br.com.easyevent.repositories.TipoMeioContatoRepository;

@Service
public class EventoService {
	
	@Autowired
	private EventoRepository eventoRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private MeioContatoRepository meioContatoRepository;
	
	@Autowired
	private TipoEventoRepository tipoEventoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private TipoMeioContatoRepository tipoMeioContatoRepository;
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Transactional
	public Evento save(Evento evento) {
		evento.setId(null);
		enderecoRepository.save(evento.getEndereco());
		meioContatoRepository.saveAll(evento.getMeiosContato());
		Evento eventoSalvo = eventoRepository.save(evento);
		return eventoSalvo;
	}

	public Evento fromDTO(EventoDTO eventoDTO) {
		Evento evento = new Evento(eventoDTO.getNome(), eventoDTO.getDescricao(), eventoDTO.getDataHoraInicio(), eventoDTO.getDataHoraTermino(), eventoDTO.getValor(), 
				eventoDTO.getMaximoDeParticipantes(), eventoDTO.getMinimoDeParticipantes(), eventoDTO.getImagemDivulgacao());
		
		Endereco endereco = new Endereco();
		endereco.setCep(eventoDTO.getCep());
		endereco.setLogradouro(eventoDTO.getLogradouro());
		endereco.setNumero(eventoDTO.getNumero());
		endereco.setBairro(eventoDTO.getBairro());
		endereco.setComplemento(eventoDTO.getComplemento());
		
		Cidade cidade = new Cidade();
		cidade = cidadeRepository.findById(eventoDTO.getCidadeId()).orElse(null);
		endereco.setCidade(cidade);
		evento.setEndereco(endereco);
		
		TipoMeioContato tipoMeioContato01 = new TipoMeioContato();
		tipoMeioContato01 = tipoMeioContatoRepository.findById(eventoDTO.getTipoMeioContato01Id()).orElse(null);
		MeioContato meioContato01 = new MeioContato();
		meioContato01.setTipoMeioContato(tipoMeioContato01);
		meioContato01.setContato(eventoDTO.getContato01());
		meioContato01.setEvento(evento);
		evento.getMeiosContato().add(meioContato01);
		
		if (eventoDTO.getTipoMeioContato02Id() != null) {
			TipoMeioContato tipoMeioContato02 = new TipoMeioContato();
			tipoMeioContato02 = tipoMeioContatoRepository.findById(eventoDTO.getTipoMeioContato02Id()).orElse(null);
			MeioContato meioContato02 = new MeioContato();
			meioContato02.setTipoMeioContato(tipoMeioContato02);
			meioContato02.setContato(eventoDTO.getContato02());
			meioContato02.setEvento(evento);
			evento.getMeiosContato().add(meioContato02);
		}
		
		if (eventoDTO.getTipoMeioContato03Id() != null) {
			TipoMeioContato tipoMeioContato03 = new TipoMeioContato();
			tipoMeioContato03 = tipoMeioContatoRepository.findById(eventoDTO.getTipoMeioContato03Id()).orElse(null);
			MeioContato meioContato03 = new MeioContato();
			meioContato03.setTipoMeioContato(tipoMeioContato03);
			meioContato03.setContato(eventoDTO.getContato03());
			meioContato03.setEvento(evento);
			evento.getMeiosContato().add(meioContato03);
		}
		
		TipoEvento tipoEvento = new TipoEvento();
		tipoEvento = tipoEventoRepository.findById(eventoDTO.getTipoEventoId()).orElse(null);
		evento.setTipoEvento(tipoEvento);
		
		Empresa empresa = new Empresa();
		empresa = empresaRepository.findById(eventoDTO.getEmpresaOrganizadoraId()).orElse(null);
		evento.setEmpresaOrganizadora(empresa);
		
		return evento;
	}
	
	public Page<Evento> findByEmpresaOrganizadora(Long id, PageRequest page) {
		Empresa empresa = empresaRepository.findById(id).orElse(null);
		Page<Evento> eventos = eventoRepository.findByEmpresaOrganizadora(empresa, page);
		return eventos;
	}

	public Page<Evento> findByNomeEvento(String nome, PageRequest page) {
		return eventoRepository.findByNomeContaining(nome, page);
	}
}
