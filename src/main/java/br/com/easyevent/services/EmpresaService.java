package br.com.easyevent.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.easyevent.domain.Empresa;
import br.com.easyevent.repositories.EmpresaRepository;

@Service
public class EmpresaService {

	@Autowired
	private BCryptPasswordEncoder passEncoder;
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	public Empresa save(Empresa empresa) {
		empresa.setId(null);
		String senhaEncoded = passEncoder.encode(empresa.getSenha());
		empresa.setSenha(senhaEncoded);
		empresaRepository.save(empresa);
		return empresa;
	}
}
