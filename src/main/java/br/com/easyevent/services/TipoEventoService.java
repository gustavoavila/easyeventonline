package br.com.easyevent.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.easyevent.domain.TipoEvento;
import br.com.easyevent.repositories.TipoEventoRepository;

@Service
public class TipoEventoService {
	
	@Autowired
	private TipoEventoRepository tipoEventoRepository;

	public List<TipoEvento> buscarTodos() {
		return tipoEventoRepository.findAll();
	}
}
