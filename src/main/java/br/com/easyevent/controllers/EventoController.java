package br.com.easyevent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.easyevent.assemblers.EventoResourceAssembler;
import br.com.easyevent.domain.Evento;
import br.com.easyevent.dto.EventoDTO;
import br.com.easyevent.resources.EventoResource;
import br.com.easyevent.services.EventoService;

@RepositoryRestController
@RequestMapping(value = "/eventos")
public class EventoController {
	
	@Autowired
	private EventoService eventoService;

	@PostMapping
	public ResponseEntity<EventoResource> save(@RequestBody EventoDTO eventoDTO) {
		Evento evento = eventoService.fromDTO(eventoDTO);
		evento = eventoService.save(evento);
		EventoResource eventoResource = new EventoResourceAssembler().toResource(evento);
		return new ResponseEntity<>(eventoResource, HttpStatus.OK);
	}
	
	@GetMapping("/empresa/{id}")
	public ResponseEntity<Resources<EventoResource>> findByEmpresaOrganizadora(@PathVariable("id") Long id) {
		PageRequest page = PageRequest.of(0, 12, Sort.by("dataHoraInicio").descending());
		List<Evento> eventos = eventoService.findByEmpresaOrganizadora(id, page).getContent();
		List<EventoResource> eventosResourcesList = new EventoResourceAssembler().toResources(eventos);
		Resources<EventoResource> eventosResources = new Resources<EventoResource>(eventosResourcesList);
		return new ResponseEntity<>(eventosResources, HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<Resources<EventoResource>> findByNomeEvento(@RequestParam(name = "nome", required = false, defaultValue = "") String nome) {
		PageRequest page = PageRequest.of(0, 12, Sort.by("dataHoraInicio").descending());
		List<Evento> listaEventos = eventoService.findByNomeEvento(nome, page).getContent();
		List<EventoResource> listaEventosResources = new EventoResourceAssembler().toResources(listaEventos);
		Resources<EventoResource> eventosResources = new Resources<EventoResource>(listaEventosResources);
		return new ResponseEntity<>(eventosResources, HttpStatus.OK);
	}
}
