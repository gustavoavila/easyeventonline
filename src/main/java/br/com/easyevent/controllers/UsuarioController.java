package br.com.easyevent.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.easyevent.assemblers.UsuarioResourceAssembler;
import br.com.easyevent.domain.Usuario;
import br.com.easyevent.resources.UsuarioResource;
import br.com.easyevent.services.UsuarioService;

@RepositoryRestController
@RequestMapping(value = "/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping
	public ResponseEntity<UsuarioResource> save(@RequestBody Usuario usuario) {
		Usuario usuarioSalvo = usuarioService.save(usuario);
		UsuarioResource usuarioResource = new UsuarioResourceAssembler().toResource(usuarioSalvo);
		return new ResponseEntity<>(usuarioResource, HttpStatus.OK);
	}
}
