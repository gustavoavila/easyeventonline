package br.com.easyevent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.easyevent.domain.TipoEvento;
import br.com.easyevent.services.TipoEventoService;

@RepositoryRestController
@RequestMapping(value = "/tiposeventos")
public class TipoEventoController {

	@Autowired
	private TipoEventoService tipoEventoService;

	@GetMapping("/todos")
	public ResponseEntity<List<TipoEvento>> buscarTodos() {
		List<TipoEvento> tiposEventos = tipoEventoService.buscarTodos();
		return new ResponseEntity<>(tiposEventos, HttpStatus.OK);
	}

}
