package br.com.easyevent.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.easyevent.domain.Cidade;
import br.com.easyevent.services.CidadeService;

@RepositoryRestController
@RequestMapping(value = "/cidades")
public class CidadeController {
	
	@Autowired
	private CidadeService cidadeService;
	
	@GetMapping("/ibge/{codigo}")
	public ResponseEntity<Cidade> findByCodigoIBGE(@PathVariable("codigo") Integer codigo) {
		Cidade cidade = cidadeService.findByCodigoIBGE(codigo);
		return new ResponseEntity<>(cidade, HttpStatus.OK);
	}

}
