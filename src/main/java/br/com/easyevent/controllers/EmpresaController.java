package br.com.easyevent.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.easyevent.assemblers.EmpresaResourceAssembler;
import br.com.easyevent.domain.Empresa;
import br.com.easyevent.resources.EmpresaResource;
import br.com.easyevent.services.EmpresaService;

@RepositoryRestController
@RequestMapping(value = "/empresas")
public class EmpresaController {

	@Autowired
	private EmpresaService empresaService;

	@PostMapping
	public ResponseEntity<EmpresaResource> save(@RequestBody Empresa empresa) {
		Empresa empresaSalva = empresaService.save(empresa);
		EmpresaResource empresaResource = new EmpresaResourceAssembler().toResource(empresaSalva);
		return new ResponseEntity<>(empresaResource, HttpStatus.OK);
	}
}
