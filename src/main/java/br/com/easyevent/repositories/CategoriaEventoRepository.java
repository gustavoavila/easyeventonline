package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.CategoriaEvento;

@RepositoryRestResource(collectionResourceRel = "categoriaseventos", path = "categoriaseventos")
public interface CategoriaEventoRepository extends JpaRepository<CategoriaEvento, Long> {

}
