package br.com.easyevent.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.Empresa;
import br.com.easyevent.domain.Evento;

@RepositoryRestResource(collectionResourceRel = "eventos", path = "eventos")
public interface EventoRepository extends JpaRepository<Evento, Long> {

	Page<Evento> findByEmpresaOrganizadora(Empresa empresa, Pageable page);

	Page<Evento> findByNomeContaining(String nome, Pageable page);
}
