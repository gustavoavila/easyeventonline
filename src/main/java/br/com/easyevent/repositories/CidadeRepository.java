package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.Cidade;

@RepositoryRestResource(collectionResourceRel = "cidades", path = "cidades")
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	Cidade findByCodigoIBGE(Integer codigo);

}
