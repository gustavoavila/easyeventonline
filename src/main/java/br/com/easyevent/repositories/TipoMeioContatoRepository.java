package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.TipoMeioContato;

@RepositoryRestResource(collectionResourceRel = "tiposmeioscontatos", path = "tiposmeioscontatos")
public interface TipoMeioContatoRepository extends JpaRepository<TipoMeioContato, Long> {

}
