package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.TipoEvento;

@RepositoryRestResource(collectionResourceRel = "tiposeventos", path = "tiposeventos")
public interface TipoEventoRepository extends JpaRepository<TipoEvento, Long> {

}
