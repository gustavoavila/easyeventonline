package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.Participante;

@RepositoryRestResource(collectionResourceRel = "participantes", path = "participantes")
public interface ParticipanteRepository extends JpaRepository<Participante, Long> {

}
