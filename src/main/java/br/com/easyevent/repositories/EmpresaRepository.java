package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.Empresa;

@RepositoryRestResource(collectionResourceRel = "empresas", path = "empresas")
public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

}
