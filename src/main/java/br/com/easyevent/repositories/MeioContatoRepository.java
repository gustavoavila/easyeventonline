package br.com.easyevent.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.easyevent.domain.MeioContato;

@RepositoryRestResource(collectionResourceRel = "meioscontatos", path = "meioscontatos")
public interface MeioContatoRepository extends JpaRepository<MeioContato, Long> {

}
