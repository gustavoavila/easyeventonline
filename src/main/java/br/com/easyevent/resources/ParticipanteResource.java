package br.com.easyevent.resources;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import br.com.easyevent.assemblers.EmpresaResourceAssembler;
import br.com.easyevent.assemblers.EnderecoResourceAssembler;
import br.com.easyevent.assemblers.EventoResourceAssembler;
import br.com.easyevent.assemblers.MeioContatoResourceAssembler;
import br.com.easyevent.domain.Participante;
import lombok.Getter;

@Relation(value = "participante", collectionRelation = "participantes")
public class ParticipanteResource extends ResourceSupport {

	private static final MeioContatoResourceAssembler meioContatoAssembler = new MeioContatoResourceAssembler();

	private static final EnderecoResourceAssembler enderecoAssembler = new EnderecoResourceAssembler();

	private static final EmpresaResourceAssembler empresaAssembler = new EmpresaResourceAssembler();

	private static final EventoResourceAssembler eventoAssembler = new EventoResourceAssembler();

	@Getter
	private String nomeUsuario;

	@Getter
	private String senha;

	@Getter
	private String imagemPerfil;

	@Getter
	private List<MeioContatoResource> meiosContato;

	@Getter
	private List<EnderecoResource> enderecos;

	@Getter
	private String nomeCompleto;

	@Getter
	private String cpf;

	@Getter
	private LocalDateTime dataNascimento;

	@Getter
	private List<EmpresaResource> empresas = new ArrayList<>();

	@Getter
	private List<EventoResource> eventos = new ArrayList<>();

	public ParticipanteResource(Participante participante) {
		this.nomeUsuario = participante.getNomeUsuario();
		this.senha = participante.getSenha();
		this.imagemPerfil = participante.getImagemPerfil();
		this.meiosContato = meioContatoAssembler.toResources(participante.getMeiosContato());
		this.enderecos = enderecoAssembler.toResources(participante.getEnderecos());
		this.nomeCompleto = participante.getNomeCompleto();
		this.cpf = participante.getCpf();
		this.dataNascimento = participante.getDataNascimento();
		this.empresas = empresaAssembler.toResources(participante.getEmpresas());
		this.eventos = eventoAssembler.toResources(participante.getEventos());
	}
}
