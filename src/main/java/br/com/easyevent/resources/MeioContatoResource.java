package br.com.easyevent.resources;

import org.springframework.hateoas.ResourceSupport;

import br.com.easyevent.domain.MeioContato;
import br.com.easyevent.domain.TipoMeioContato;
import lombok.Getter;

public class MeioContatoResource extends ResourceSupport {
	
	@Getter
	private TipoMeioContato tipoMeioContato;

	@Getter
	private String contato;
	
	public MeioContatoResource(MeioContato meioContato) {
		this.tipoMeioContato = meioContato.getTipoMeioContato();
		this.contato = meioContato.getContato();
	}
}
