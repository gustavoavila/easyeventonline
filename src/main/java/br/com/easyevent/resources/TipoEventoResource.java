package br.com.easyevent.resources;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import br.com.easyevent.assemblers.CategoriaEventoResourceAssembler;
import br.com.easyevent.domain.TipoEvento;
import lombok.Getter;

@Relation(value = "tipoevento", collectionRelation = "tiposeventos")
public class TipoEventoResource extends ResourceSupport {

	private static final CategoriaEventoResourceAssembler categoriaEventoAssembler = new CategoriaEventoResourceAssembler();
	
	@Getter
	private String descricao;
	
	@Getter
	private CategoriaEventoResource categoria;

	public TipoEventoResource(TipoEvento tipoEvento) {
		this.descricao = tipoEvento.getDescricao();
		this.categoria = categoriaEventoAssembler.toResource(tipoEvento.getCategoria());
	}
	
}
