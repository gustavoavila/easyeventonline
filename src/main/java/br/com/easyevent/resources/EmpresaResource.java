package br.com.easyevent.resources;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import br.com.easyevent.assemblers.EnderecoResourceAssembler;
import br.com.easyevent.assemblers.MeioContatoResourceAssembler;
import br.com.easyevent.domain.Empresa;
import lombok.Getter;

@Relation(value = "empresa", collectionRelation = "empresas")
public class EmpresaResource extends ResourceSupport {
	
	private static final MeioContatoResourceAssembler meioContatoAssembler = new MeioContatoResourceAssembler();

	private static final EnderecoResourceAssembler enderecoAssembler = new EnderecoResourceAssembler();

	@Getter
	private final String nomeUsuario;

	@Getter
	private final String senha;
	
	@Getter
	private final String imagemPerfil;

	@Getter
	private final List<MeioContatoResource> meiosContato;

	@Getter
	private final List<EnderecoResource> enderecos;
	
	@Getter
	private String razaoSocial;

	@Getter
	private String nomeFantasia;

	@Getter
	private String cnpj;

	@Getter
	private String cnae;

	public EmpresaResource(Empresa empresa) {
		this.nomeUsuario = empresa.getNomeUsuario();
		this.senha = empresa.getSenha();
		this.imagemPerfil = empresa.getImagemPerfil();
		this.meiosContato = meioContatoAssembler.toResources(empresa.getMeiosContato());
		this.enderecos = enderecoAssembler.toResources(empresa.getEnderecos());
		this.razaoSocial = empresa.getRazaoSocial();
		this.nomeFantasia = empresa.getNomeFantasia();
		this.cnpj = empresa.getCnpj();
		this.cnae = empresa.getCnae();
	}

}
