package br.com.easyevent.resources;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import br.com.easyevent.domain.CategoriaEvento;
import lombok.Getter;

@Relation(value = "categoriaevento", collectionRelation = "categoriaseventos")
public class CategoriaEventoResource extends ResourceSupport {
	
	@Getter
	private String descricao;
	
	public CategoriaEventoResource(CategoriaEvento categoriaEvento) {
		this.descricao = categoriaEvento.getDescricao();
	}
}
