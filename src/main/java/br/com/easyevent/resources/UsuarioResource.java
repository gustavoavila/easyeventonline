package br.com.easyevent.resources;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import br.com.easyevent.assemblers.EnderecoResourceAssembler;
import br.com.easyevent.assemblers.MeioContatoResourceAssembler;
import br.com.easyevent.domain.Usuario;
import lombok.Getter;

@Relation(value = "usuario", collectionRelation = "usuarios")
public class UsuarioResource extends ResourceSupport {

	private static final MeioContatoResourceAssembler meioContatoAssembler = new MeioContatoResourceAssembler();

	private static final EnderecoResourceAssembler enderecoAssembler = new EnderecoResourceAssembler();

	@Getter
	private final String nomeUsuario;

	@Getter
	private final String senha;
	
	@Getter
	private final String imagemPerfil;

	@Getter
	private final List<MeioContatoResource> meiosContato;

	@Getter
	private final List<EnderecoResource> enderecos;

	public UsuarioResource(Usuario usuario) {
		this.nomeUsuario = usuario.getNomeUsuario();
		this.senha = usuario.getSenha();
		this.imagemPerfil = usuario.getImagemPerfil();
		this.meiosContato = meioContatoAssembler.toResources(usuario.getMeiosContato());
		this.enderecos = enderecoAssembler.toResources(usuario.getEnderecos());
	}

}
