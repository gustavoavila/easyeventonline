package br.com.easyevent.resources;

import org.springframework.hateoas.ResourceSupport;

import br.com.easyevent.domain.Cidade;
import br.com.easyevent.domain.Endereco;
import lombok.Getter;

public class EnderecoResource extends ResourceSupport {

	@Getter
	private String logradouro;

	@Getter
	private Integer numero;

	@Getter
	private String bairro;

	@Getter
	private String complemento;

	@Getter
	private String cep;

	@Getter
	private Cidade cidade;

	public EnderecoResource(Endereco endereco) {
		this.logradouro = endereco.getLogradouro();
		this.numero = endereco.getNumero();
		this.bairro = endereco.getBairro();
		this.complemento = endereco.getComplemento();
		this.cep = endereco.getCep();
		this.cidade = endereco.getCidade();
	}

}
