package br.com.easyevent.resources;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import br.com.easyevent.assemblers.EmpresaResourceAssembler;
import br.com.easyevent.assemblers.EnderecoResourceAssembler;
import br.com.easyevent.assemblers.MeioContatoResourceAssembler;
import br.com.easyevent.assemblers.ParticipanteResourceAssembler;
import br.com.easyevent.assemblers.TipoEventoResourceAssembler;
import br.com.easyevent.domain.Evento;
import lombok.Getter;

@Relation(value = "evento", collectionRelation = "eventos")
public class EventoResource extends ResourceSupport {

	private static final EnderecoResourceAssembler enderecoAssembler = new EnderecoResourceAssembler();
	private static final MeioContatoResourceAssembler meioContatoAssembler = new MeioContatoResourceAssembler();
	private static final TipoEventoResourceAssembler tipoEventoAssembler = new TipoEventoResourceAssembler();
	private static final EmpresaResourceAssembler empresaAssembler = new EmpresaResourceAssembler();
	private static final ParticipanteResourceAssembler participanteAssembler = new ParticipanteResourceAssembler();

	@Getter
	private String nome;

	@Getter
	private String descricao;

	@Getter
	private LocalDateTime dataHoraInicio;

	@Getter
	private LocalDateTime dataHoraTermino;

	@Getter
	private BigDecimal valor;

	@Getter
	private Integer maximoDeParticipantes;

	@Getter
	private Integer minimoDeParticipantes;

	@Getter
	private String facilitador;

	@Getter
	private Integer classificacao;

	@Getter
	private String observacoes;

	@Getter
	private String imagemDivulgacao;

	@Getter
	private EnderecoResource endereco;

	@Getter
	private List<MeioContatoResource> meiosContato = new ArrayList<>();

	@Getter
	private TipoEventoResource tipoEvento;

	@Getter
	private EmpresaResource empresaOrganizadora;

	@Getter
	private List<ParticipanteResource> participantesInteressados = new ArrayList<>();

	public EventoResource(Evento evento) {
		this.nome = evento.getNome();
		this.descricao = evento.getDescricao();
		this.dataHoraInicio = evento.getDataHoraInicio();
		this.dataHoraTermino = evento.getDataHoraTermino();
		this.valor = evento.getValor();
		this.maximoDeParticipantes = evento.getMaximoDeParticipantes();
		this.minimoDeParticipantes = evento.getMinimoDeParticipantes();
		this.facilitador = evento.getFacilitador();
		this.classificacao = evento.getClassificacao();
		this.observacoes = evento.getObservacoes();
		this.imagemDivulgacao = evento.getImagemDivulgacao();
		this.endereco = enderecoAssembler.toResource(evento.getEndereco());
		this.meiosContato = meioContatoAssembler.toResources(evento.getMeiosContato());
		this.tipoEvento = tipoEventoAssembler.toResource(evento.getTipoEvento());
		this.empresaOrganizadora = empresaAssembler.toResource(evento.getEmpresaOrganizadora());
		this.participantesInteressados = participanteAssembler.toResources(evento.getParticipantesInteressados());
	}

}
