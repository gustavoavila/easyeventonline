package br.com.easyevent.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cidade")
public class Cidade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "nome", nullable = false, length = 100)
	private String nome;
	
	@Column(name = "codigo_ibge", nullable = false)
	private Integer codigoIBGE;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "estado_id", referencedColumnName = "id", nullable = false)
	private Estado estado;

}
