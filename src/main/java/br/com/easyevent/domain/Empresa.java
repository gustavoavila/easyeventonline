package br.com.easyevent.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "empresa")
@PrimaryKeyJoinColumn(name = "id")
public class Empresa extends Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "razaosocial", length = 100)
	private String razaoSocial;

	@Column(name = "nomefantasia", length = 100)
	private String nomeFantasia;

	@Column(name = "cnpj", length = 14)
	private String cnpj;

	@Column(name = "cnae", length = 7)
	private String cnae;

	@ManyToMany(mappedBy = "empresas")
	private List<Participante> participantesInteressados = new ArrayList<>();

}
