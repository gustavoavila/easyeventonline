package br.com.easyevent.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "participante")
@PrimaryKeyJoinColumn(name = "id")
public class Participante extends Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "nome_completo", nullable = false, length = 100)
	private String nomeCompleto;

	@Column(name = "cpf", length = 11)
	private String cpf;

	@Column(name = "datanascimento")
	private LocalDateTime dataNascimento;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Empresa> empresas = new ArrayList<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Evento> eventos = new ArrayList<>();
	
	public void addEmpresa(Empresa empresa) {
		empresas.add(empresa);
		empresa.getParticipantesInteressados().add(this);
	}

	public void removeEmpresa(Empresa empresa) {
		empresas.remove(empresa);
		empresa.getParticipantesInteressados().remove(this);
	}

	public void addEvento(Evento evento) {
		eventos.add(evento);
		evento.getParticipantesInteressados().add(this);
	}

	public void removeEvento(Evento evento) {
		eventos.remove(evento);
		evento.getParticipantesInteressados().remove(this);
	}

}
