package br.com.easyevent.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "usuario")
@Inheritance(strategy = InheritanceType.JOINED)
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "nomeusuario", nullable = false, length = 20)
	private String nomeUsuario;

	@Column(name = "senha", nullable = false)
	private String senha;
	
	@Lob
	@Column(name = "imagemperfil")
	private String imagemPerfil;
	
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.REFRESH)
	private List<MeioContato> meiosContato = new ArrayList<>();
	
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.REFRESH)
	private List<Endereco> enderecos = new ArrayList<>();
	
	public void addMeioContato(MeioContato meioContato) {
		meiosContato.add(meioContato);
		meioContato.setUsuario(this);
	}

	public void removeMeioContato(MeioContato meioContato) {
		meiosContato.remove(meioContato);
		meioContato.setUsuario(null);
	}
	
	public void addEndereco(Endereco endereco) {
		enderecos.add(endereco);
		endereco.setUsuario(this);
	}

	public void removeEndereco(Endereco endereco) {
		enderecos.remove(endereco);
		endereco.setUsuario(null);
	}
}
