package br.com.easyevent.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "meiocontato")
public class MeioContato implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "tipomeiocontato_id", referencedColumnName = "id", nullable = false)
	private TipoMeioContato tipoMeioContato;

	@Column(name = "contato", nullable = false)
	private String contato;
	
	@ManyToOne
	private Usuario usuario;
	
	@ManyToOne
	private Evento evento;

}
