package br.com.easyevent.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "evento")
public class Evento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "nome", nullable = false, length = 80)
	@NonNull
	private String nome;
	
	@Column(name = "descricao", nullable = false, length = 255)
	@NonNull
	private String descricao;

	@Column(name = "datahorainicio", nullable = false)
	@NonNull
	private LocalDateTime dataHoraInicio;

	@Column(name = "datahoratermino", nullable = false)
	@NonNull
	private LocalDateTime dataHoraTermino;

	@Column(name = "valor", nullable = false)
	@NonNull
	private BigDecimal valor;

	@Column(name = "maximodeparticipantes", nullable = false)
	@NonNull
	private Integer maximoDeParticipantes;

	@Column(name = "minimodeparticipantes", nullable = false)
	@NonNull
	private Integer minimoDeParticipantes;

	@Column(name = "facilitador", length = 100)
	private String facilitador;

	@Column(name = "classificacao")
	private Integer classificacao;

	@Column(name = "observacoes", length = 255)
	private String observacoes;

	@Lob
	@Column(name = "imagemdivulgacao")
	@NonNull
	private String imagemDivulgacao;

	@OneToOne
	@JoinColumn(name = "endereco_id", referencedColumnName = "id", nullable = false)
	//@NonNull
	private Endereco endereco;

	@OneToMany(mappedBy = "evento", cascade = CascadeType.ALL)
	//@NonNull
	private List<MeioContato> meiosContato = new ArrayList<>();

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "tipoevento_id", referencedColumnName = "id", nullable = false)
	//@NonNull
	private TipoEvento tipoEvento;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "empresa_id", referencedColumnName = "id", nullable = false)
	//@NonNull
	private Empresa empresaOrganizadora;

	@ManyToMany(mappedBy = "eventos")
	private List<Participante> participantesInteressados = new ArrayList<>();
	
	public void addMeioContato(MeioContato meioContato) {
		meiosContato.add(meioContato);
		meioContato.setEvento(this);
	}

	public void removeMeioContato(MeioContato meioContato) {
		meiosContato.remove(meioContato);
		meioContato.setEvento(null);
	}
}
