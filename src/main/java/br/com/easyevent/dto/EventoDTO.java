package br.com.easyevent.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class EventoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String descricao;
	private LocalDateTime dataHoraInicio;
	private LocalDateTime dataHoraTermino;
	private BigDecimal valor;
	private Integer maximoDeParticipantes;
	private Integer minimoDeParticipantes;
	private String facilitador;
	private Integer classificacao;
	private String observacoes;
	private String imagemDivulgacao;
	private Long empresaOrganizadoraId;
	
	private String logradouro;
	private Integer numero;
	private String bairro;
	private String complemento;
	private String cep;
	private Long cidadeId;
	
	private Long tipoMeioContato01Id;
	private String contato01;
	private Long tipoMeioContato02Id;
	private String contato02;
	private Long tipoMeioContato03Id;
	private String contato03;
	
	private Long tipoEventoId;
}
