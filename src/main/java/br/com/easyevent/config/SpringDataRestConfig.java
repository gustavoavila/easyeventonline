package br.com.easyevent.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

import br.com.easyevent.domain.Usuario;

@Configuration
public class SpringDataRestConfig {

	@Bean
	public ResourceProcessor<PagedResources<Resource<Usuario>>> usuarioProcessor(EntityLinks links) {
		
		return new ResourceProcessor<PagedResources<Resource<Usuario>>>() {
			public PagedResources<Resource<Usuario>> process(PagedResources<Resource<Usuario>> resource) {
				resource.add(links.linkFor(Usuario.class).withRel("usuarios"));
				return resource;
			}
		};
	}
}
